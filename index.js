"use strict";

const urlStreamer = require("./url-streamer");
const replaceSheet = require("./sheets");
const querystring = require("querystring");

async function collectIssues(project, queryParams, formatter) {
  let params = querystring.stringify(queryParams);
  let encodedProjectUri = encodeURIComponent(project);
  let url = `https://gitlab.com/api/v4/projects/${encodedProjectUri}/issues?${params}`;

  let results = [];

  for await (const issue of urlStreamer(url)) {
    results.push(formatter(issue));
  }

  return results;
}

function csvFormatter(format) {
  return function (issue) {
    return Object.keys(format).reduce(function (memo, key) {
      let result = format[key].call(null, issue);
      memo[key] = result;
      return memo;
    }, {});
  };
}

function transpose(formatObject, rows) {
  let result = [Object.keys(formatObject)]; // header row
  rows.forEach((row) => result.push(Object.values(row)));
  return result;
}

module.exports = async function gitlabIssuesToSheet({ project, queryParams, formatObject, sheetId, sheetRange }) {
  let issues = await collectIssues(project, queryParams, csvFormatter(formatObject));
  let transposed = transpose(formatObject, issues);

  await replaceSheet(sheetId, sheetRange, transposed);
};
